# /********************************************************************************
# * Copyright © 2018-2020, ETH Zurich, D-BSSE, Andreas P. Cuny & Gotthold Fläschner
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the GNU Public License v3.0
# * which accompanies this distribution, and is available at
# * http://www.gnu.org/licenses/gpl
# *
# * Contributors:
# *     Aaron Ponti - initial API
# *     Andreas P. Cuny - initial API and final implementation
# *******************************************************************************/

from PyQt5.QtCore import QPointF
from pyIMD.ui.poscorrection.line import Line
from pyIMD.ui.poscorrection.vertex import Vertex


class CompositeLine:
    """Composite line.

    This is not a QGraphicsItem and cannot be added to a QGraphicsScene directly.
    If a QGraphicsScene is passed to it in the constructor or as an argument to
    addToScene(), it will manage a series of QGraphicsItems (one 'Line' and two
    'Vertex' items) and their spatial relationships.
    """

    def __init__(self, pos: QPointF = QPointF(400.0, 300.0), scene=None):
        """Constructor."""

        # Initial position
        self.pos = pos

        # Keep track of which object is being actively moved; since the others
        # will follow programmatically, but will still fire their itemChanged
        # events
        self._selectedItem = None

        # Keep track of last position during a move
        self._lastPosition = None

        # Store the scene
        self._scene = scene

        # Defaults
        self._diameter = 2
        self._line_length = 50

        # Relevant coordinates
        self._x0 = pos.x() - self._line_length / 2
        self._y0 = pos.y()
        self._x = pos.x() + self._line_length / 2
        self._y = pos.y()

        # Add the Line
        self._line = Line(self._x0, self._y0, self._x, self._y, "Line", self)

        # Add Vertex A
        self._vertexA = Vertex(self._x0, self._y0, 2.5, "A", self)

        # Add Vertex B
        self._vertexB = Vertex(self._x, self._y, 2.5, "B", self)

        # Do we have a scene already?
        if self._scene is not None:
            self.addToScene(self._scene)

    def addToScene(self, scene):
        """Add Line and Vertex objects to the scene."""
        if scene is not None:
            self._scene = scene
            self._scene.addItem(self._line)
            self._scene.addItem(self._vertexA)
            self._scene.addItem(self._vertexB)

    def centerOfMass(self):
        if self._line is None:
            return None
        p1 = self._line.line().p1()
        p2 = self._line.line().p2()
        c = QPointF(0.5 * (p1.x() + p2.x()), 0.5 * (p1.y() + p2.y()))
        return self._line.mapToScene(c)

    def itemMovedTo(self, item, newPos):
        """Called when the passed Item has been moved."""
        if self._selectedItem is not item:
            return

        # Calculate delta
        delta = newPos - self._lastPosition

        if item is self._vertexA:
            # Only update the first point of the line
            line = self._line.line()
            self._line.setLine(
                line.p1().x() + delta.x(),
                line.p1().y() + delta.y(),
                line.p2().x(),
                line.p2().y())
        elif item is self._vertexB:
            # Only update the second point of the line
            line = self._line.line()
            self._line.setLine(
                line.p1().x(),
                line.p1().y(),
                line.p2().x() + delta.x(),
                line.p2().y() + delta.y())
        elif item is self._line:
            # Move both vertices
            self._vertexA.setPos(self._vertexA.scenePos() + delta)
            self._vertexB.setPos(self._vertexB.scenePos() + delta)
        else:
            pass

        # Update the last position
        self._lastPosition = newPos

    def setSelectedItemAndOrigin(self, item, originScenePos):
        self._selectedItem = item

        # Store positions at the beginning of a move
        if item is None:
            self._lastPosition = None
        else:
            self._lastPosition = originScenePos