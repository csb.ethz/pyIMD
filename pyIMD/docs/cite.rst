.. highlight:: shell

How to cite
-----------

If you use pyIMD in your academic work we would appreciate if you cite us. To do so please use:

.. code-block:: bibtex

	@article{Cuny2019,
            title = {pyIMD: Automated analysis of inertial mass measurements of single cells},
            journal = {SoftwareX},
            volume = {10},
            pages = {100303},
            year = {2019},
            issn = {2352-7110},
            doi = {https://doi.org/10.1016/j.softx.2019.100303},
            url = {https://www.sciencedirect.com/science/article/pii/S2352711019300871},
            author = {Andreas P. Cuny and David Martínez-Martín and Gotthold Fläschner},
            keywords = {Single cell, Mass, Picobalance, Oscillators},
            abstract = {The total mass of single cells can be accurately monitored in real time under physiological conditions with our recently developed picobalance. It is a powerful tool to investigate crucial processes in biophysics, cell biology or medicine, such as cell growth or hydration dynamics. However, processing of the raw data can be challenging, as computation is needed to extract the mass and long-term measurements can generate large amounts of data. Here, we introduce the software package pyIMD that automates raw data processing, particularly when investigating non-migrating cells. pyIMD is implemented in Python and can be used as a command line tool or as a stand-alone version including a graphical user interface.}
            }